// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "ProjectileDefault.h"
#include "FuncLibrary/Types.h"

#include "WeaponDefault.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFireStart, UAnimMontage*, AnimFireChar);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart, UAnimMontage*, AnimReloadChar);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, bool, bIsSuccess, int32, AmmoSafe);

UCLASS()
class TDS_PROJECT_API AWeaponDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponDefault();

	FOnWeaponFireStart OnWeaponFireStart;
	FOnWeaponReloadStart OnWeaponReloadStart;
	FOnWeaponReloadEnd OnWeaponReloadEnd;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Components")
		class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Components")
		class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Components")
		class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Components")
		class UArrowComponent* ShootLocation = nullptr;

	UPROPERTY() 
		FWeaponInfo WeaponSetting;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponInfo")
		FAddicionalWeaponInfo WeaponInfo;

	//Timers
	    float FireTimer = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
	    float ReloadTimer = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogicDebug")
		float ReloadTimeDebugLogic = 0.0f;

	//Flags
	    bool BlockFire = false;

	//Dispersion
		bool ShoulReduceDespersion = false;
		float CurrentDispersion = 0.0f;
		float CurrentDispersionMax = 1.0f;
		float CurrentDispersionMin = 0.1f;
		float CurrentDispersionRecoil = 0.1f;
		float CurrentDispersionReduction = 0.1f;
		
	//Timer Drop Magazine on Reload
		bool DropClipFlag = false;
		float DropClipTimer = -1.0f;

	//Shell Flag
		bool DropShellFlag = false;
		float DropShellTimer = -1.0f;

		FVector ShootEndLocation = FVector(0);

	//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
		bool WeaponReloading = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
		bool WeaponFiring = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	    bool WeaponAiming = false;

	//DEBUG
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool ShowDebug = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		float SizeVectorToChangeShootDirectionLogic = 100.0f;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	    void FireTick(float DeltaTIme);
	    void ReloadTick(float DeltaTime);
		void DispersionTick(float DeltaTime);
		void ClipDropTick(float DeltaTime);
		void ShellDropTick(float DeltaTime);

	    void WeaponInit();

	UFUNCTION(BlueprintCallable)
		void SetWeaponStateFire(bool bIsFire);

	    bool CheckWeaponCanFire();

	FProjectileInfo GetProgectile();

	    void Fire();

	    void UpdateStateWeapon(EMovementState NewMovementState);
	    void ChangeDispersion();
		void ChangeDispersionByShot();

		float GetCurrentDispersion() const;
		FVector ApplyDispersionToShoot(FVector DirectionShoot) const;
		FVector GetFireEndLocation() const;
		int8 GetNumperProjectileByShot() const;

	UFUNCTION(BlueprintCallable)
	    int32 GetWeaponRound();

	    void InitReload();
	    void FinishReload();
		void CancelReload();

	UFUNCTION(NetMulticast, Unreliable)
		void AnimWeaponStart_Multicast(UAnimMontage* Anim); //21.08.23
	
	UFUNCTION()
		void InitDropMesh(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass);
	

};
