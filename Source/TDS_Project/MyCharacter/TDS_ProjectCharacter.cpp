// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_ProjectCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "../ProjectileDefault.h"
#include "../Games/TDSGameInstance.h"
#include "Kismet/KismetMathLibrary.h"


ATDS_ProjectCharacter::ATDS_ProjectCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 1000.f;
	CameraBoom->SetRelativeRotation(FRotator(-80.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	//CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	//CursorToWorld->SetupAttachment(RootComponent);
	//static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	//if (DecalMaterialAsset.Succeeded())
	//{
		//CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	//}
	//CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	//CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATDS_ProjectCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	//if (CursorToWorld != nullptr)
	//{
		//if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		//{
			//if (UWorld* World = GetWorld())
			//{
				//FHitResult HitResult;
				//FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				//FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				//FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				//Params.AddIgnoredActor(this);
				//World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				//FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				//CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			//}
		//}
	   if (CurrentCursor)
	   {
		 APlayerController * PC = Cast<APlayerController>(GetController());
		 if(PC && PC->IsLocalPlayerController())
		 {
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		 }
	   }
	MovementTick(DeltaSeconds);
}

void ATDS_ProjectCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (!IsInputActive)
	{
		InitWeapon(InitWeaponName);

	}
	else
		CurrentWeapon->Destroy();
	
	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}
}

void ATDS_ProjectCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATDS_ProjectCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATDS_ProjectCharacter::InputAxisY);
	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATDS_ProjectCharacter::InputAttackPressed);
	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATDS_ProjectCharacter::InputAttackReleased);
	NewInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ATDS_ProjectCharacter::TryReloadWeapon);
	NewInputComponent->BindAction(TEXT("Weapon"), EInputEvent::IE_Released, this, &ATDS_ProjectCharacter::ChoiceOfWeaponName);

}

void ATDS_ProjectCharacter::InputAttackPressed()
{
	AttackCharEvent(true);
}

void ATDS_ProjectCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ATDS_ProjectCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATDS_ProjectCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATDS_ProjectCharacter::MovementTick(float DeltaTime)
{

	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

	

	APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	
	if (MyController)
	{
		FHitResult ResultHit;
		//MyController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);
		MyController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);

		float FindRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
		SetActorRotation(FQuat(FRotator(0.0f, FindRotatorResultYaw, 0.0f)));

		if (CurrentWeapon)
		{
			FVector Dispalacement = FVector(0);
			
			switch(MovementState)
			{
			case EMovementState::Aim_State:
				Dispalacement = FVector(0.0f, 0.0f, 160.0f);
				CurrentWeapon->ShoulReduceDespersion = true;
				break;
			case EMovementState::AimWalk_State:
				Dispalacement = FVector(0.0f, 0.0f, 160.0f);
				CurrentWeapon->ShoulReduceDespersion = true;
				break;
			case EMovementState::Walk_State:
				Dispalacement = FVector(0.0f, 0.0f, 120.0f);
				CurrentWeapon->ShoulReduceDespersion = false;
				break;
			case EMovementState::Run_State:
				Dispalacement = FVector(0.0f, 0.0f, 120.0f);
				CurrentWeapon->ShoulReduceDespersion = false;
				break;
			case EMovementState::SprintRun_State:
				break;
			case EMovementState::FlashSprint_State:
				break;
			default:
				break;
			}
			CurrentWeapon->ShootEndLocation = ResultHit.Location + Dispalacement;
			//aim cursor 3d widget;

		}

	}

}

void ATDS_ProjectCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->SetWeaponStateFire(bIsFiring);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ATDS_ProjectCharacter::AttackCharEvent - CurrentWeapon -NULL"));
}

void ATDS_ProjectCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;

	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementSpeedInfo.AimSpeedNormal;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementSpeedInfo.WalkSpeedNormal;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementSpeedInfo.RunSpeedNormal;
		break;
	case EMovementState::SprintRun_State:
		ResSpeed = MovementSpeedInfo.SprintRunSpeedRun;
		break;
	case EMovementState::FlashSprint_State:
		ResSpeed = MovementSpeedInfo.FlashSprint;
		break;
	default:
		break;
	}
	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;

}

void ATDS_ProjectCharacter::ChangeMovementState()
{

	if (!WalkEnabled && !SprintRunEnabled && !AimEnabled)
	{
		MovementState = EMovementState::Run_State;
	}
	if (SprintRunEnabled)
	{
		WalkEnabled = false;
		AimEnabled = false;
		MovementState = EMovementState::SprintRun_State;
	}
	else
	{
		if (WalkEnabled && !SprintRunEnabled && AimEnabled)
		{
			MovementState = EMovementState::AimWalk_State;
		}
		else
		{
			if (WalkEnabled && !SprintRunEnabled && !AimEnabled)
			{
				MovementState = EMovementState::Walk_State;
			}
			else
			{
				if (!WalkEnabled && !SprintRunEnabled && AimEnabled)
				{
					MovementState = EMovementState::Aim_State;
				}


			}
		}
		
	}
	CharacterUpdate();

	//Weapon state Update
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon(MovementState);
	}
	
}

AWeaponDefault* ATDS_ProjectCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

UDecalComponent* ATDS_ProjectCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

void ATDS_ProjectCharacter::InitWeapon(FName IdWeaponName)
{
	UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		if(myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WSRHand"));
					CurrentWeapon = myWeapon;

					myWeapon->WeaponSetting = myWeaponInfo;
					myWeapon->WeaponInfo.Round = myWeaponInfo.MaxRound;
					//Remove
					myWeapon->ReloadTimeDebugLogic = myWeaponInfo.ReloadTime;
					myWeapon->UpdateStateWeapon(MovementState);

					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATDS_ProjectCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATDS_ProjectCharacter::WeaponReloadEnd);
					myWeapon->OnWeaponFireStart.AddDynamic(this, &ATDS_ProjectCharacter::WeaponFireStart);
			
				}

			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ATDS_ProjectCharacter::InitWeapon - Weapon not found in table -NULL"));
		}
	}

	
}

void ATDS_ProjectCharacter::SetMovementState_Multicast_Implementation(EMovementState NewState)
{
	MovementState = NewState;
	CharacterUpdate();
}

EMovementState ATDS_ProjectCharacter::GetMovementState()
{
	return EMovementState();
}

void ATDS_ProjectCharacter::TryReloadWeapon()
{
	if (CurrentWeapon)
	{
		if (CurrentWeapon->GetWeaponRound() <= CurrentWeapon->WeaponSetting.MaxRound)
		{
			CurrentWeapon->InitReload();
		}
	}
}

void ATDS_ProjectCharacter::PlayAnim_Multicast_Implementation(UAnimMontage* Anim)
{
	if (GetMesh() && GetMesh()->GetAnimInstance())
	{
		GetMesh()->GetAnimInstance()->Montage_Play(Anim);
	}
}

void ATDS_ProjectCharacter::WeaponFireStart(UAnimMontage* Anim)
{
	WeaponFireStart_BP(Anim);
}


void ATDS_ProjectCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

void ATDS_ProjectCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe)
{
	WeaponReloadEnd_BP();
}

void ATDS_ProjectCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
}

void ATDS_ProjectCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
}

void ATDS_ProjectCharacter::WeaponReloadEnd_BP_Implementation()
{
}

void ATDS_ProjectCharacter::ChoiceOfWeaponName()
{
	IsInputActive = true;
	if (IsInputActive)
	{
		if (CurrentWeapon != nullptr)
		{
			CurrentWeapon->Destroy();
		}
		else
		CurrentWeapon = nullptr;
		InitWeapon(InitWeaponName);
		
	}
	
	IsInputActive = false;
}
