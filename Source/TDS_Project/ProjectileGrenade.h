// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ProjectileDefault.h"

#include "ProjectileGrenade.generated.h"
/**
 * 
 */
UCLASS()
class TDS_PROJECT_API AProjectileGrenade : public AProjectileDefault
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

	void TimerExplose(float DeltaTime);

	virtual void BulletCollisionSphereHit(class UPrimitiveComponent* HitComp,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		FVector NormalImpulse,
		const FHitResult& Hit);

	virtual void ImpactProjectile() override;

	UFUNCTION()
	void Explose();
	UFUNCTION(BlueprintNativeEvent)
	void Explose_BP();


	bool TimerEnabled = false;
	float TimerToExplose = 0.0f;
	float TimeToExplose = 5.0f;
	
};
